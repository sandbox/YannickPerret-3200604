# G-Jammer module
This module allows to hide or desactivate fields when editing contents, based on several criteria.

## Configuration
The configuration page for the module (`admin/config/user-interface/jammer-config`) allows to set rules
to be applied. Each rule has the following elements:

* Form id: drop-down to select on which form the rule will apply
* Elements id: list of fields name (comma-separated) in the form on which the rule will apply
  * note that just below you can find a list of all available fields (sorted by form) to help getting names
* Remove/disable: if checked the target field(s) are no more visible on the form; else they are swtiched to
  read-only state (note: this may have no effect on some type of fields)
* Always/Exclude creation/Only creation/Exclude when empty: allow to apply the rule depending of the creation
  status of the node (if creating a new node or modifying an existing node)
  * note that "Exclude when empty" is not implemented
* Only apply to role(s): list of role(s) which are concerned by the rule. The rule is applied if current user
  has (at least) one of these roles
* Reverse role(s): negate the previous behavior. If checked the rule is applied if current user do not have
  any of these roles
* Priority: from -20 (max priority) to 20. Rules are evaluated in this order
* Category/group: string used to group together existing rules in the list at the end of the page
* Comment: comment that can be associated to the rule

"Existing actions" is a table that lists defined rules. Rules are sorted by "Category" (in alphabetical
order), and the first category is "Unsorted" (for all rules without category). Each line contains:
* the form id
* the field(s) id
* the remove/disable state
* the "exception" state
* the role(s) list
* the "reverse role(s)" state
* the priority
* the comment
* a link to modify the given rule
* a link to delete the given rule

## Editing flow
When creating a new rule: fill all the required fields and use "Validate" button.

When modifying a rule: all the values are pre-filled in fields. Perform your modifications and use "Validate" button.

When deleting a rule: a confirmation form is displayed. Use "Confirm" to delete, or use "go back" to cancel.

In each case a message should indicate that the operation has been performed (or you should get an error
message).

## Depends / compatibility
G-Jammer has been tested on the 9.x series.

It has no dependency, but it sets "chosen" for the drop-downs in the configuration form. Whatever it can
be used without "chosen" installed.

## Files structure
* jammer/
  * jammer.info.yml: the module declaration
  * jammer.links.menu.yml: declaration of configuration form
  * jammer.permissions.yml: 
  * jammer.routing.yml: declaration of routes/handlers
  * jammer.module: implements `HOOK_form_alter()` that performs actions of fields when editing forms
  * README.md: this file
  * config/
    * schema/
      * jammer.settings.yml: declaration of the configuration used by the module
    * install/
      * jammer.settings.yml: default value of the configuration
  * templates/
    * src/
      * Form/
        * GConfigForm.php: the configuration form (build, submit, and various tools)

## Technical elements
Configuration is stored as a serialized PHP array in the module's configuration. Note: maybe a better
way exists.

The `buildForm()` function is the same for all actions (view, create, modify, delete). Depending on the
route parameters (action + id, or nothing) it adapts itself:
* no parameter: print configuration form with nothing filled
* modify+id: print configuration form with all fields filled with 'id' rule
* delete+id: print a confirm form

The `submitForm()` function is the same for all actions:
* no parameter: create a new rule in configuration based on form data
* modify+id: update rule targeted by 'id' in configuration based on form data
* delete+id: delete rule targeted by 'id' in configuration based on form data

Some sanity checks are performed in both. In all cases a message (info, warning, error) is generated
and user is redirected to the main configuration page.

The `HOOK_form_alter()` loops on the existing rules. For each:
* if the form_id matches
 * if the user roles matches
  * if the exclusion criteria matches
   * for each target fields in rule
    * apply the corresponding modification (hide or read-only)

The `jammer.module` also implements the associated `help` page.

